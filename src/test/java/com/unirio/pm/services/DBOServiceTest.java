package com.unirio.pm.services;

import com.unirio.pm.domain.Cartao;
import com.unirio.pm.domain.Cobranca;
import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class DBOServiceTest {

    @Test
    public void deveCriarCiclistaPorId() {
        Cobranca cobranca = Cobranca.builder().id("1").build();
        DBOService.addCobranca(cobranca);
        Cobranca dboCobranca = DBOService.getCobranca(cobranca);
        assertEquals(cobranca.getId(), dboCobranca.getId());
    }

    @Test
    public void deveRemoverCiclistaPorId() {
        Cobranca cobranca = Cobranca.builder().id("1").build();
        DBOService.addCobranca(cobranca);
        assertEquals(true, DBOService.deleteCobranca(cobranca));
        assertEquals(null, DBOService.getCobranca(cobranca));
    }

    @Test
    public void deveRealizarCobranca() {
        Cobranca cobranca = Cobranca.builder().id("1").build();
        DBOService.addCobranca(cobranca);
        assertEquals(true, DBOService.realizaCobranca(cobranca));
    }

    @Test
    public void deveVerificarEmailExiste() {
        assertEquals(false, DBOService.verificaEmailExiste("email"));
    }

    @Test
    public void deveNotificarEmail() {
        assertEquals(true, DBOService.notificaEmail("email@email.com"));
    }

    @Test
    public void deveValidarCartaoCredito() {
        assertEquals(true, DBOService.validaCartaoCredito(Cartao.builder().id("1").build()));
    }
}
