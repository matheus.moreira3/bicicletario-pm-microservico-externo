package com.unirio.pm.controllers;

import com.unirio.pm.domain.Cartao;
import controllers.CartaoController;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CartaoControllerTest {
    private CartaoController cartaoController;

    @Before
    public void setup() {
        this.cartaoController = new CartaoController();
    }

    @Test
    public void deveCriarCartao() {
        String id = "23";
        String cvv = "359";
        String nome = "Matheus C G Moreira";
        String num = "23523553";
        String validade = "20/11/2020";
        Cartao cartao = Cartao.builder().id(id).cvv(cvv).nomeTitular(nome).numero(num).validade(validade).build();
        assertEquals(id, cartao.getId());
        assertEquals(cvv, cartao.getCvv());
        assertEquals(nome, cartao.getNomeTitular());
        assertEquals(num, cartao.getNumero());
        assertEquals(validade, cartao.getValidade());

    }

    @Test
    public void deveValidarCartao() {
        assertEquals(true, this.cartaoController.validaCartaoCredito(Cartao.builder().build()));
    }
}
