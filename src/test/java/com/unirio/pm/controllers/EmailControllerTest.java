package com.unirio.pm.controllers;

import com.unirio.pm.domain.Email;
import com.unirio.pm.domain.Error;
import com.unirio.pm.domain.NovoEmail;
import controllers.EmailController;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EmailControllerTest {
    private EmailController emailController;

    @Before
    public void setup() {
        this.emailController = new EmailController();
    }

    @Test
    public void deveCriarEmail() {
        String email = "teste@teste.com";
        String mensagem = "teste";
        String id = "123";
        Email em = Email.builder().mail(email).mensagem(mensagem).uuid(id).build();
        assertEquals(email, em.getMail());
        assertEquals(mensagem, em.getMensagem());
        assertEquals(id, em.getUuid());
    }

    @Test
    public void deveCriarNovoEmail() {
        String email = "teste@teste.com";
        String mensagem = "teste";
        NovoEmail em = NovoEmail.builder().email(email).mensagem(mensagem).build();
        assertEquals(email, em.getEmail());
        assertEquals(mensagem, em.getMensagem());
    }

    @Test
    public void deveCriarError() {
        String id = "123";
        String cod = "422";
        String men = "mensagem";
        Error err = Error.builder().id(id).codigo(cod).mensagem(men).build();
        assertEquals(id, err.getId());
        assertEquals(cod, err.getCodigo());
        assertEquals(men, err.getMensagem());
    }

    @Test
    public void deveVerificarEmailExiste() {
        String email = "test@test.com";
        this.emailController.setEmail(email);
        assertEquals(true, this.emailController.verificaEmailExiste(email));
    }

    @Test
    public void deveVerificarEmailNãoExiste() {
        assertEquals(false, this.emailController.verificaEmailExiste("test@test.com"));
    }

    @Test
    public void deveNotificarEmail() {
        String email = "test@test.com";
        assertEquals(true, this.emailController.notificaEmail(email));
    }

    @Test
    public void deveDarErroPatternEmail() {
        String email = "test";
        assertEquals(false, this.emailController.notificaEmail(email));
    }
}
