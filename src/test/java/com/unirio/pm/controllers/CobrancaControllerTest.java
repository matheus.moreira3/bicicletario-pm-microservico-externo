package com.unirio.pm.controllers;

import com.unirio.pm.domain.Cobranca;
import com.unirio.pm.domain.PagamentoStatus;
import controllers.CobrancaController;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CobrancaControllerTest {
	private CobrancaController cobrancaController;

	@Before
	public void setup() {
		this.cobrancaController = new CobrancaController();
	}

	@Test
	public void deveCriarNovaCobranca() {
		this.cobrancaController.addCobranca(Cobranca.builder().id("1").ciclista("12423523").horaFinalizacao("12:00").horaSolicitacao("20:00").status(PagamentoStatus.PENDENTE).build());
		assertEquals(1, this.cobrancaController.getAllCobrancas().size());
	}

	@Test
	public void deveDeletarCobranca() {
		this.cobrancaController.addCobranca(Cobranca.builder().id("1").status(PagamentoStatus.PAGA).build());
		boolean ciclistaDeletado = this.cobrancaController.deleteCobranca("1");
		assertEquals(true, ciclistaDeletado);
		assertEquals(null, this.cobrancaController.getCobranca("1"));
	}

	@Test
	public void deveDarErroAoDeletarCobranca() {
		this.cobrancaController.addCobranca(Cobranca.builder().status(PagamentoStatus.CANCELADA).build());
		boolean ciclistaDeletado = this.cobrancaController.deleteCobranca("1");
		assertEquals(false, ciclistaDeletado);
	}

	@Test
	public void devePegarTodasCobrancas() {
		assertEquals(0, this.cobrancaController.getAllCobrancas().size());
	}

	@Test
	public void deveRealizarCobranca() {
		this.cobrancaController.addCobranca(Cobranca.builder().id("1").status(PagamentoStatus.FALHA).build());
		assertEquals(true, this.cobrancaController.realizaCobranca(Cobranca.builder().id("1").build()));
	}
	
}
