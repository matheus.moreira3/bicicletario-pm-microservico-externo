package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailController {
    private List<String> emails = new ArrayList<>();
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static EmailController emailController;

    public static EmailController getEmailController() {
        if (emailController == null) {
            emailController = new EmailController();
        }
        return emailController;
    }

    private boolean validaPatternEmail(String email) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }

    public void setEmail(String email) {
        this.emails.add(email);
    }

    public boolean verificaEmailExiste(String email) {
        return this.emails.contains(email);
    }

    public boolean notificaEmail(String email) {
        return this.validaPatternEmail(email);
    }
}
