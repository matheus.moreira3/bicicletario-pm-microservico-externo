package controllers;


import com.fasterxml.uuid.Generators;
import com.unirio.pm.domain.Cobranca;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CobrancaController {
	private List<Cobranca> cobrancas = new ArrayList<>();
	private static CobrancaController cobrancaController;
	
	public static CobrancaController getCobrancaController() {
		if (cobrancaController == null) {
			cobrancaController = new CobrancaController();
		}
		return cobrancaController;
	}

	private Cobranca geraNovaCobranca(Cobranca cobranca) {
		if (cobranca.getId() == null) {
			cobranca.setId(Generators.timeBasedGenerator().generate().toString());
		}
		if (cobranca.getHoraSolicitacao() == null) {
			cobranca.setHoraSolicitacao(new Date().toString());
		}
		return cobranca;
	}

	public List<Cobranca> getAllCobrancas() {
		return this.cobrancas;
	}

	public Cobranca getCobranca(String id) {
		for(int i=0; i <= this.cobrancas.size() - 1; i++) {
			if (this.cobrancas.get(i).getId().equals(id)) {
				return this.cobrancas.get(i);
			}
		}
		return null;
	}

	public boolean realizaCobranca(Cobranca cobranca) {
		boolean removido = this.deleteCobranca(cobranca.getId());
		cobranca.setHoraFinalizacao(new Date().toString());
		return removido;
	}

	public boolean addCobranca(Cobranca c) {
		this.geraNovaCobranca(c);
		if (this.getCobranca(c.getId()) == null) {
			return this.cobrancas.add(c);
		} else {
			return false;
		}
	}

	public boolean deleteCobranca(String id) {
		for(int i=0; i <= this.cobrancas.size() - 1; i++) {
			if (this.cobrancas.get(i).getId().equals(id)) {
				this.cobrancas.remove(i);
				return true;
			}
		}
		return false;
	}

}
