package com.unirio.pm;

import com.unirio.pm.domain.*;
import com.unirio.pm.domain.Error;
import com.unirio.pm.services.DBOService;
import io.javalin.Javalin;


public class Server {
	private final static String PATH = "/cobranca";
	private final static String NOT_FOUND_MESSAGE = "Não encontrado";
	private final static String INVALID_DATA_MESSAGE = "Dados Inválidos";
	private final static String INVALID_EMAIL = "E-mail com formato inválido";

	private static int getHerokuAssignedPort() {
		String herokuPort = System.getenv("PORT");
		if (herokuPort != null) {
			return Integer.parseInt(herokuPort);
		}
		return 7000;
	}

	public static void main (String[] args) {
		Javalin app = Javalin.create().start(getHerokuAssignedPort());
        app.get(PATH + "/:id", ctx -> {
        	String id = ctx.pathParam("id");
			Cobranca cobranca = DBOService.getCobranca(Cobranca.builder().id(id).build());
			if (cobranca != null) {
				ctx.status(200).json(cobranca);
			} else {
				ctx.status(404).json(Error.builder().codigo("404").mensagem(NOT_FOUND_MESSAGE).build());
			}
        });

		app.post(PATH, ctx -> {
			Cobranca cobranca = ctx.bodyAsClass(Cobranca.class);
			boolean cobrado = DBOService.realizaCobranca(cobranca);
			if (cobrado) {
				ctx.status(200).json(cobranca);
			} else {
				ctx.status(422).json(Error.builder().codigo("422").mensagem(INVALID_DATA_MESSAGE).build());
			}
		});

		app.post("/enviarEmail", ctx -> {
			NovoEmail email = ctx.bodyAsClass(NovoEmail.class);
			if (!DBOService.verificaEmailExiste(email.getEmail())) {
				ctx.status(404).json(Error.builder().codigo("404").mensagem(NOT_FOUND_MESSAGE).build());
				return;
			}
			boolean notificado = DBOService.notificaEmail(email.getEmail());
			if (notificado) {
				ctx.status(200).json(Email.builder().mail(email.getEmail()).mensagem(email.getMensagem()).build());
			} else {
				ctx.status(422).json(Error.builder().codigo("422").mensagem(INVALID_EMAIL));
			}
		});

		app.post("/filaCobranca", ctx -> {
			Cobranca cobranca = ctx.bodyAsClass(Cobranca.class);
			boolean adicionado = DBOService.addCobranca(cobranca);
			if (adicionado) {
				ctx.status(200).json(cobranca);
			} else {
				ctx.status(422).json(Error.builder().codigo("422").mensagem(INVALID_DATA_MESSAGE).build());
			}
		});

		app.post("/validaCartaoDeCredito", ctx -> {
			Cartao cartao = ctx.bodyAsClass(Cartao.class);
			boolean validado = DBOService.validaCartaoCredito(cartao);
			if (validado) {
				ctx.status(200).json(null);
			} else {
				ctx.status(422).json(Error.builder().codigo("422").mensagem(INVALID_DATA_MESSAGE).build());
			}
		});
	}
}
