package com.unirio.pm.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Email {

    @JsonProperty("uuid")
    private String uuid;

    @JsonProperty("email")
    private String mail;

    @JsonProperty("mensagem")
    private String mensagem;
}
