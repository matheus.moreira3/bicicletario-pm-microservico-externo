package com.unirio.pm.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class Cobranca {
    @JsonProperty("id")
    private String id;

    @JsonProperty("status")
    private PagamentoStatus status;

    @JsonProperty("horaSolicitacao")
    private String horaSolicitacao;

    @JsonProperty("horaFinalizacao")
    private String horaFinalizacao;

    @JsonProperty("valor")
    private Integer valor;

    @JsonProperty("ciclista")
    private String ciclista;
}
