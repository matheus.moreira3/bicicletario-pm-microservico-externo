package com.unirio.pm.services;

import com.unirio.pm.domain.Cartao;
import com.unirio.pm.domain.Cobranca;
import controllers.CartaoController;
import controllers.CobrancaController;
import controllers.EmailController;

public class DBOService {

	private static CobrancaController cobrancaController;
	private static EmailController emailController;
	private static CartaoController cartaoController;
	private static void initCobrancaController() {
		cobrancaController = CobrancaController.getCobrancaController();
	}
	private static void initEmailController() {
		emailController = EmailController.getEmailController();
	}
	private static void initCartaoController() { cartaoController = CartaoController.getCartaoController(); }

	private DBOService() {
		initCartaoController();
		initCobrancaController();
		initEmailController();
	}


	public static Cobranca getCobranca(Cobranca cobranca) {
		initCobrancaController();
		return cobrancaController.getCobranca(cobranca.getId());
	}

	public static boolean addCobranca(Cobranca cobranca) {
		initCobrancaController();
		return cobrancaController.addCobranca(cobranca);
	}

	public static boolean deleteCobranca(Cobranca cobranca) {
		initCobrancaController();
		return cobrancaController.deleteCobranca(cobranca.getId());
	}

	public static boolean realizaCobranca(Cobranca cobranca) {
		initCobrancaController();
		return cobrancaController.realizaCobranca(cobranca);
	}

	public static boolean verificaEmailExiste(String email) {
		initEmailController();
		return emailController.verificaEmailExiste(email);
	}

	public static boolean notificaEmail(String email) {
		initEmailController();
		return emailController.notificaEmail(email);
	}

	public static boolean validaCartaoCredito(Cartao cartao) {
		initCartaoController();
		return cartaoController.validaCartaoCredito(cartao);
	}
}
